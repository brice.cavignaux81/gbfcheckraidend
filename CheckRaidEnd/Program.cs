﻿// See https://aka.ms/new-console-template for more information

using System;
using System.Drawing;
using System.IO;
using System.Threading;
using CheckRaidEnd;
using H.Utilities;

Console.WriteLine("Hello, just checking if your NM95 is over!");
Console.WriteLine("Just make sure this window is focused (click on it) and press any key to leave!");

if (!File.Exists("Images/raidimage.png") || !File.Exists("Images/fail.png"))
{
    Console.WriteLine("Missing file 'Images/raidimage.png' or 'Images/fail.png', press any key to leave.");
    Console.ReadKey();
}
else
{
    var application = new MyProgram();

    application.Run();
}


public class MyProgram
{
    public MyProgram()
    {
    }

    public void Run()
    {
        CancellationTokenSource cts = new();
        ThreadPool.QueueUserWorkItem(Loop, cts.Token);
        Console.ReadKey();
        cts.Cancel();
        Thread.Sleep(500);
        cts.Dispose();    
    }

    private static void Loop(object? obj)
    {
        var token = (CancellationToken) obj;
        using var img1 = new Bitmap(@"Images/raidimage.png");
        using var imgFail = new Bitmap(@"Images/fail.png");
        while (!token.IsCancellationRequested)
        {
            using var img2 = Screenshoter.Shot();

            var rect = img2.FindImageOnBitmap(img1, false);
            var rectFail = img2.FindImageOnBitmap(imgFail, false);
            if (!rect.IsEmpty || !rectFail.IsEmpty) Console.Beep();

            Thread.Sleep(500);
        }
    }
    
}
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace CheckRaidEnd
{
    public static class BitmapExtension
    {
        /// <summary>
        /// Returns a Rectangle with the coordinates of the image in the <paramref name="fullBitmap"/>. If the image is not found, the returned Rectangle will be Rectangle.Empty
        /// Code found here https://social.msdn.microsoft.com/Forums/windows/en-US/41fd70cf-22e9-47f6-9c63-1e4fbd2af5b3/how-do-i-find-a-pictureimage-on-the-screen-and-get-the-xy-coordinates-cnet?forum=csharpgeneral
        /// </summary>
        /// <param name="fullBitmap"></param>
        /// <param name="bmpMatch"></param>
        /// <param name="exactMatch"></param>
        /// <returns></returns>
        public static Rectangle FindImageOnBitmap(this Bitmap fullBitmap, Bitmap bmpMatch, bool exactMatch)
        {
            BitmapData imgBmd = bmpMatch.LockBits(new Rectangle(0, 0, bmpMatch.Width, bmpMatch.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);
            BitmapData screenBmd = fullBitmap.LockBits(new Rectangle(0, 0, fullBitmap.Width, fullBitmap.Height), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            byte[] imgBytes = new byte[Math.Abs(imgBmd.Stride) * bmpMatch.Height - 1 + 1];
            byte[] screenBytes = new byte[Math.Abs(screenBmd.Stride) * fullBitmap.Height - 1 + 1];

            Marshal.Copy(imgBmd.Scan0, imgBytes, 0, imgBytes.Length);
            Marshal.Copy(screenBmd.Scan0, screenBytes, 0, screenBytes.Length);

            var rct = Rectangle.Empty;

            var skpx = Convert.ToInt32((bmpMatch.Width - 1) / (double) 10);
            if ((skpx < 1) | exactMatch)
                skpx = 1;
            var skpy = Convert.ToInt32((bmpMatch.Height - 1) / (double) 10);
            if ((skpy < 1) | exactMatch)
                skpy = 1;

            for (var si = 0; si <= screenBytes.Length - 1; si += 3)
            {
                var foundMatch = true;
                for (var iy = 0; iy <= imgBmd.Height - 1; iy += skpy)
                for (var ix = 0; ix <= imgBmd.Width - 1; ix += skpx)
                {
                    var sindx = iy * screenBmd.Stride + ix * 3 + si;
                    var iindx = iy * imgBmd.Stride + ix * 3;
                    var spc = Color.FromArgb(screenBytes[sindx + 2], screenBytes[sindx + 1], screenBytes[sindx]).ToArgb();
                    var ipc = Color.FromArgb(imgBytes[iindx + 2], imgBytes[iindx + 1], imgBytes[iindx]).ToArgb();
                    if (spc == ipc) continue;
                    foundMatch = false;
                    iy = imgBmd.Height - 1;
                    ix = imgBmd.Width - 1;
                }

                if (!foundMatch) continue;
                var r = si / (double) (fullBitmap.Width * 3);
                var c = fullBitmap.Width * (r % 1);
                if (r % 1 >= 0.5)
                    r -= 1;
                rct.X = Convert.ToInt32(c);
                rct.Y = Convert.ToInt32(r);
                rct.Width = bmpMatch.Width;
                rct.Height = bmpMatch.Height;
                break;
            }

            bmpMatch.UnlockBits(imgBmd);
            fullBitmap.UnlockBits(screenBmd);
            return rct;
        }
    }
}